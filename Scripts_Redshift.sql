
--------------------------------------------------------------------

-- 1  CRIAR ESSAS TABELAS NO READSHIFT --- https://docs.aws.amazon.com/redshift/latest/gsg/rs-gsg-create-sample-db.html
create table users(
	userid integer not null distkey sortkey,
	username char(8),
	firstname varchar(30),
	lastname varchar(30),
	city varchar(30),
	state char(2),
	email varchar(100),
	phone char(14),
	likesports boolean,
	liketheatre boolean,
	likeconcerts boolean,
	likejazz boolean,
	likeclassical boolean,
	likeopera boolean,
	likerock boolean,
	likevegas boolean,
	likebroadway boolean,
	likemusicals boolean);
  
  create table venue(
	venueid smallint not null distkey sortkey,
	venuename varchar(100),
	venuecity varchar(30),
	venuestate char(2),
	venueseats integer);

create table category(
	catid smallint not null distkey sortkey,
	catgroup varchar(10),
	catname varchar(10),
	catdesc varchar(50));

 
 create table date(
	dateid smallint not null distkey sortkey,
	caldate date not null,
	day character(3) not null,
	week smallint not null,
	month character(5) not null,
	qtr character(5) not null,
	year smallint not null,
	holiday boolean default('N'));

                            
   create table event(
	eventid integer not null distkey,
	venueid smallint not null,
	catid smallint not null,
	dateid smallint not null sortkey,
	eventname varchar(200),
	starttime timestamp);


create table listing(
	listid integer not null distkey,
	sellerid integer not null,
	eventid integer not null,
	dateid smallint not null  sortkey,
	numtickets smallint not null,
	priceperticket decimal(8,2),
	totalprice decimal(8,2),
	listtime timestamp);


create table sales(
	salesid integer not null,
	listid integer not null distkey,
	sellerid integer not null,
	buyerid integer not null,
	eventid integer not null,
	dateid smallint not null sortkey,
	qtysold smallint not null,
	pricepaid decimal(8,2),
	commission decimal(8,2),
	saletime timestamp);






-- 2

---AWS DEMO  Carregue dados de amostra do Amazon S3 usando o comando COPY. Faça download do arquivo tickitdb.zip , que contém arquivos de dados de amostra individuais.

-- Descompacte e carregue os arquivos individuais em uma tickitpasta em seu bucket do Amazon S3 em sua região da AWS.

-- Edite os comandos COPY neste tutorial para apontar para os arquivos em seu bucket do Amazon S3. Para obter informações sobre como gerenciar arquivos com o Amazon S3, consulte Criar e configurar um bucket do S3 no Guia do usuário do Amazon Simple Storage Service .

-- Forneça autenticação para que seu cluster acesse o Amazon S3 em seu nome para carregar os dados de amostra. Você fornece autenticação referenciando a função do IAM que você criou e definiu como padrão para seu cluster nas etapas anteriores.

-- Os comandos COPY incluem um espaço reservado para o nome de recurso da Amazon (ARN) para a função IAM, seu nome de bucket e uma região da AWS, conforme mostrado no exemplo a seguir.
 
-- https://docs.aws.amazon.com/redshift/latest/gsg/rs-gsg-create-sample-db.html

copy user from 's3://awssampledbuswest2/tickit/category_pipe.txt'
iam_role 'arn:aws:iam::<aws-account-id>:role/<role-name>'
region 'us-west-2';


copy venue from 's3://awssampledbuseast2ep/tickit/venue_pipe.txt' 
iam_role 'arn:aws:iam::<aws-account-id>:role/<role-name>'
delimiter '|' region 'us-east-2';



copy category from 's3://awssampledbuseast2ep/tickit/category_pipe.txt' 
iam_role 'arn:aws:iam::<aws-account-id>:role/<role-name>'
delimiter '|' region 'us-east-2';



copy date from 's3://awssampledbuseast2ep/tickit/date2008_pipe.txt' 
iam_role 'arn:aws:iam::<aws-account-id>:role/<role-name>'
delimiter '|' region 'us-east-2';



copy event from 's3://awssampledbuseast2ep/tickit/allevents_pipe.txt' 
iam_role 'arn:aws:iam::<aws-account-id>:role/<role-name>'
delimiter '|' timeformat 'YYYY-MM-DD HH:MI:SS' region 'us-east-2';



copy listing from 's3://awssampledbuseast2ep/tickit/listings_pipe.txt' 
iam_role 'arn:aws:iam::<aws-account-id>:role/<role-name>'
delimiter '|' region 'us-east-2';



copy sales from 's3://awssampledbuseast2ep/tickit/sales_tab.txt'
iam_role 'arn:aws:iam::<aws-account-id>:role/<role-name>'
delimiter '\t' timeformat 'MM/DD/YYYY HH:MI:SS' region 'us-east-2';



create external schema myspectrum_schema 
from data catalog 
database 'myspectrum_db' 
iam_role 'arn:aws:iam::<aws-account-id>:role/<role-name>'
create external database if not exists;


--3 criar um esquema externo, substitua o ARN da função IAM no seguinte comando pelo ARN da função que você criou na etapa 1 . Em seguida, execute o comando em seu cliente SQL.

create external schema myspectrum_schema 
from data catalog 
database 'myspectrum_db' 
iam_role 'arn:aws:iam::<aws-account-id>:role/<role-name>'
create external database if not exists;

--Para criar uma tabela externa, execute o seguinte comando CREATE EXTERNAL TABLE.
--Observação
-- Seu cluster e o bucket do Amazon S3 devem estar na mesma região da AWS. Para este exemplo de comando CREATE EXTERNAL TABLE, o bucket do Amazon S3 com os dados de amostra está localizado na região da AWS Leste dos EUA (N. Virgínia). Para ver os dados de origem, baixe o sales_ts.000arquivo.

create external table myspectrum_schema.sales(
salesid integer,
listid integer,
sellerid integer,
buyerid integer,
eventid integer,
dateid smallint,
qtysold smallint,
pricepaid decimal(8,2),
commission decimal(8,2),
saletime timestamp)
row format delimited
fields terminated by '\t'
stored as textfile
location 's3://awssampledbuseast2ep/tickit/spectrum/sales/'
table properties ('numRows'='172000');


-- 4 consultar seus dados no Amazon S3 Depois que suas tabelas externas são criadas, você pode consultá-las usando as mesmas instruções SELECT que usa para consultar outras tabelas do Amazon Redshift. Essas consultas de instrução SELECT incluem junção de tabelas, agregação de dados e filtragem de predicados.

-- Para consultar seus dados no Amazon S3
-- Obtenha o número de linhas na tabela MYSPECTRUM_SCHEMA.SALES.
select count(*) from myspectrum_schema.sales;


--- RESULTADO esperado
cont
------
172462

--- O exemplo a seguir associa a tabela externa do Amazon S3 MYSPECTRUM_SCHEMA.SALES à tabela local do Amazon Redshift EVENT para encontrar o total de vendas dos 10 principais eventos.
select top 10 myspectrum_schema.sales.eventid, sum(myspectrum_schema.sales.pricepaid) from myspectrum_schema.sales, event
where myspectrum_schema.sales.eventid = event.eventid
and myspectrum_schema.sales.pricepaid > 30
group by myspectrum_schema.sales.eventid
order by 2 desc;

--- RESULTADO esperado
evento | soma      
--------+--------- 
    289 | 51846.00 
   7895 | 51049.00 
   1602 | 50301.00 
    851 | 49956.00 
   7315 | 49823.00 
   6471 | 47997.00 
   2118 | 47863.00 
    984 | 46780.00 
   7851 | 46661.00 
   5638 | 46280,00


--- 5: experimente consultas de exemplo usando o editor de consultas --- https://docs.aws.amazon.com/redshift/latest/gsg/rs-gsg-try-query.html
-- Get definition for the sales table.
SELECT *    
FROM pg_table_def    
WHERE tablename = 'sales';    

-- Find total sales on a given calendar date.
SELECT sum(qtysold) 
FROM   sales, date 
WHERE  sales.dateid = date.dateid 
AND    caldate = '2008-01-05';

-- Find top 10 buyers by quantity.
SELECT firstname, lastname, total_quantity 
FROM   (SELECT buyerid, sum(qtysold) total_quantity
        FROM  sales
        GROUP BY buyerid
        ORDER BY total_quantity desc limit 10) Q, users
WHERE Q.buyerid = userid
ORDER BY Q.total_quantity desc;

-- Find events in the 99.9 percentile in terms of all time gross sales.
SELECT eventname, total_price 
FROM  (SELECT eventid, total_price, ntile(1000) over(order by total_price desc) as percentile 
       FROM (SELECT eventid, sum(pricepaid) total_price
             FROM   sales
             GROUP BY eventid)) Q, event E
       WHERE Q.eventid = E.eventid
       AND percentile = 1
ORDER BY total_price desc;
